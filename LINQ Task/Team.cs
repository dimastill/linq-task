﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        public static implicit operator Team(long Id)
        {
            string jsonTeams = Data.GetJson($"https://bsa2019.azurewebsites.net/api/Teams/{Id}");
            return JsonConvert.DeserializeObject<Team>(jsonTeams);
        }
    }
}
