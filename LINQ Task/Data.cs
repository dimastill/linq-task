﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    static class Data
    {
        static HttpClient client = new HttpClient();

        public static List<Project> Projects { get; set; } = new List<Project>();
        public static List<Task> Tasks { get; set; } = new List<Task>();
        public static List<User> Users { get; set; } = new List<User>();
        public static List<Team> Teams { get; set; } = new List<Team>();
        public static void LoadData()
        {
            string jsonTeams = GetJson("https://bsa2019.azurewebsites.net/api/Teams");
            Teams = JsonConvert.DeserializeObject<List<Team>>(jsonTeams);

            string jsonUsers = GetJson("https://bsa2019.azurewebsites.net/api/Users");
            Users = JsonConvert.DeserializeObject<List<User>>(jsonUsers);

            string jsonProjects = GetJson("https://bsa2019.azurewebsites.net/api/Projects");
            Projects = JsonConvert.DeserializeObject<List<Project>>(jsonProjects);

            string jsonTasks = GetJson("https://bsa2019.azurewebsites.net/api/Tasks");
            Tasks = JsonConvert.DeserializeObject<List<Task>>(jsonTasks);
        }

        public static string GetJson(string uri)
        {
            HttpResponseMessage response = client.GetAsync($"{uri}").Result;
            string json = response.Content.ReadAsStringAsync().Result;

            return json;
        }

        public static Dictionary<Project, int> GetNumberTasks(int userId)
        {
            var query = from user in Users
                         where user.Id == userId
                         join task in Tasks on user.Id equals task.Perfomer.Id
                         join project in Projects on task.Project.Id equals project.Id
                         let userProject = project
                         let numberTaskInProject = Tasks.Where(t => t.Project.Id == project.Id).Count()
                         select (userProject, numberTaskInProject);

            Dictionary<Project, int> result = new Dictionary<Project, int>();
            foreach (var item in query)
            {
                result.Add(item.userProject, item.numberTaskInProject);
            }

            return result;
        }

        public static List<Task> GetListTasks(int userId)
        {
            var result = Tasks.Where(task => task.Perfomer.Id == userId && task.Name.Length < 45);

            return result.ToList();
        }

        public static object GetListTasksFinished2019(int userId)
        {
            var result = Tasks.Where(task => task.Perfomer.Id == userId)
                              .Where(task => task.State.Value == "Finished")
                              .Where(task => task.FinishedAt.Year == 2019)
                              .Select(task => new { task.Id, task.Name });

            return result;
        }

        public static IEnumerable<(int, string, IEnumerable<User>)> GetOldUsers()
        {
            var oldUsers = from user in Users
                           where user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null
                           join team in Teams on user.Team.Id equals team.Id
                           orderby user.RegisteredAt descending
                           group user by team;

            foreach (var item in oldUsers)
            {
                yield return (item.Key.Id, item.Key.Name, item.Select(user => user));
            }
        }

        public static IEnumerable<(string, string, IEnumerable<string>)> GetSortedUsersList()
        {
            var sortedUsers = from user in Users
                              orderby user.FirstName
                              join task in Tasks on user.Id equals task.Perfomer.Id
                              orderby task.Name.Length
                              group task by user;

            foreach (var item in sortedUsers)
            {
                yield return (item.Key.FirstName, item.Key.LastName, item.Select(task => task.Name));
            }
        }
        
        public static IEnumerable<(User, Project, int, int, Task)> GetInfoAboutTasks(int userId)
        {
            var result = from user in Users
                         where user.Id == userId
                         join project in Projects on user.Id equals project.Author.Id
                         where project.CreatedAt == Projects.Where(p => p.Author.Id == user.Id)
                                                            .Max(x => x.CreatedAt)
                         let lastProject = project
                         join task in Tasks on project.Id equals task.Project.Id
                         let numberTasksInLastProject = Tasks.Count(t => t.Project.Id == lastProject.Id)
                         let numberUnfinishedOrCanceledTasks = Tasks.Count(t => t.Project.Id == lastProject.Id && 
                                                                                   task.State.Value != "Finished")
                         let longTask = Tasks.Where(t => t.Project.Id == lastProject.Id)
                                             .Aggregate((x, y) => (x.FinishedAt - x.CreateAt) > 
                                                                  (y.FinishedAt - y.CreateAt) ? x : y)
                         select (user, lastProject, numberTasksInLastProject, numberUnfinishedOrCanceledTasks, longTask);

            return result;
        }

        public static IEnumerable<(Project, Task, Task, int)> GetInfoAboutProject(int projectId)
        {
            var result = from project in Projects
                         where project.Id == projectId
                         join task in Tasks on project.Id equals task.Project.Id
                         let longDescriptionProject = Tasks.Where(t => t.Project.Id == project.Id)
                                                           .Aggregate((x, y) => x.Description.Length >
                                                                                y.Description.Length ? x : y)
                         let shortNameTask = Tasks.Where(t => t.Project.Id == project.Id)
                                                           .Aggregate((x, y) => x.Name.Length <
                                                                                y.Name.Length ? x : y)
                         let tasksInProject = Tasks.Where(t => t.Project.Id == project.Id)
                         let numberUsersInProject = tasksInProject.Where(t => t.Project.Description.Length > 25 ||
                                                                              tasksInProject.Count() < 3)
                                                                  .Select(t => t.Perfomer).GroupBy(t => t.Id).Count()
                         select (project, longDescriptionProject, shortNameTask, numberUsersInProject);

            return result;
        }
    }
}
