﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    static class Menu
    {
        public static void Display()
        {
            Console.WriteLine("1. Отримати кiлькiсть таскiв у проектi конкретного користувача (по id)\n" +
                              "2. Отримати список таскiв, призначених для конкретного користувача (по id), де name таска " +
                              "<45 символiв\n" +
                              "3. Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished) в поточному (2019) " +
                              "роцi для конкретного користувача (по id)\n" +
                              "4. Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд, учасники яких " +
                              "старшi 12 рокiв, вiдсортованих за датою реєстрацiї користувача за спаданням, а також " +
                              "згрупованих по командах.\n" +
                              "5. Отримати список користувачiв за алфавiтом first_name (по зростанню) " +
                              "з вiдсортованими tasks по довжинi name (за спаданням)\n" +
                              "6. Отримати наступну структуру (передати Id користувача в параметри):\n" +
                              "\tUser\n" +
                              "\tОстаннiй проект користувача(за датою створення)\n" +
                              "\tЗагальна кiлькiсть таскiв пiд останнiм проектом\n" +
                              "\tЗагальна кiлькiсть незавершених або скасованих таскiв для користувача\n" +
                              "\tНайтривалiший таск користувача за датою(найранiше створений - найпiзнiше закiнчений)\n" +
                              "7. Отримати таку структуру (передати Id проекту в параметри):\n" +
                              "\tПроект\n" +
                              "\tНайдовший таск проекту(за описом)\n" +
                              "\tНайкоротший таск проекту(по iменi)\n" +
                              "\tЗагальна кiлькiсть користувачiв в командi проекту, де або опис проекту > 25 символiв, " +
                              "або кiлькiсть таскiв < 3");
        }

        public static int InputMenuItem()
        {
            Console.Write("Виберiть iндекс елемента меню: ");
            int menuItem = Convert.ToInt32(Console.ReadLine());

            return menuItem;
        }

        public static int InputUserId()
        {
            Console.Write("Введiть id користувача: ");
            int userId = Convert.ToInt32(Console.ReadLine());

            return userId;
        }

        public static int InputProjectId()
        {
            Console.Write("Введiть id проекту ");
            int projectId = Convert.ToInt32(Console.ReadLine());

            return projectId;
        }

        public static void DisplayNumberTasksByUserId()
        {
            int userId = InputUserId();

            Dictionary<Project, int> numberTasks = Data.GetNumberTasks(userId);

            Console.WriteLine($"Кiлькiсть завдань у проектах для користувача з id {userId}:");
            foreach (var project in numberTasks)
            {
                Console.WriteLine($"Проект: {project.Key.Name} Кiлькiсть таскiв: {project.Value}");
            }
        }

        public static void DisplayListTasksByUserId()
        {
            int userId = InputUserId();

            List<Task> tasks = Data.GetListTasks(userId);

            foreach (var task in tasks)
            {
                Console.WriteLine($"Завдання #{task.Id}: {task.Name}");
            }
        }

        public static void DisplayTasksFinishedIn2019()
        {
            int userId = InputUserId();

            dynamic tasksFinishedIn2019 = Data.GetListTasksFinished2019(userId);
            foreach (var task in tasksFinishedIn2019)
            {
                Console.WriteLine($"Id #{task.Id}. Назва таскiв: {task.Name}");
            }
        }

        public static void DisplayUsersOlderThan12YearsOld()
        {
            foreach (var oldUser in Data.GetOldUsers())
            {
                Console.Write($"Команда #{oldUser.Item1} {oldUser.Item2}\nУчасники:\n");
                foreach (var teamUser in oldUser.Item3)
                {
                    Console.WriteLine($"{teamUser.FirstName} {teamUser.LastName} " +
                        $"(Дата народження: {teamUser.Birthday.ToString("dd/MM/yyyy")}. " +
                        $"Дата реєстрацiї: {teamUser.RegisteredAt.ToString("dd/MM/yyyy")})");
                }
            }
        }

        public static void DisplaySortedUsersList()
        {
            foreach (var sortedItem in Data.GetSortedUsersList())
            {
                foreach (var task in sortedItem.Item3)
                {
                    Console.WriteLine($"{sortedItem.Item1} {sortedItem.Item2} (Таск: {task})");
                }
            }
        }

        public static void DisplayInfoAboutUser()
        {
            int userId = InputUserId();

            var response = Data.GetInfoAboutTasks(userId);
            var result = response.FirstOrDefault();
            Console.WriteLine($"Користувач #{result.Item1.Id}\n" +
                $"Останнiй проект #{result.Item2.Id}\n" +
                $"(Кiлькiсть таскiв: {result.Item3}. Незавершених/Скасованих: {result.Item4})\n" +
                $"Найтривалiший таск #{result.Item5.Id}");
        }

        public static void DisplayInfoAboutProject()
        {
            int projectId = InputProjectId();

            try
            {
                var responseAboutProject = Data.GetInfoAboutProject(projectId);
                var infoAboutProject = responseAboutProject.FirstOrDefault();
                Console.WriteLine($"Проект #{infoAboutProject.Item1.Id}\n" +
                    $"Найдовший таск (за описом) #{infoAboutProject.Item2.Id}\n" +
                    $"Найкоротший таск (по iменi) #{infoAboutProject.Item3.Id}\n" +
                    $"Кiлькiсть користувачiв в командi даного проекту: {infoAboutProject.Item4}");
            }
            catch
            {
                Console.WriteLine("Проект не має таскiв");
            }
        }

        public static void Wait()
        { 
            Console.WriteLine("\nНатиснiть будь-яку клавiшу для повернення до меню");
            Console.ReadKey();
            Console.Clear();
        }

        public static void Loading() => Console.WriteLine("Loading data...");
    }
}
