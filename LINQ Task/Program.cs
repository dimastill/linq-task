﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu.Loading();
            Data.LoadData();
            Console.Clear();

            do
            {
                Menu.Display();
                int menuItem = Menu.InputMenuItem();
                Console.Clear();

                switch (menuItem)
                {
                    case 1:
                        Menu.DisplayNumberTasksByUserId();
                        break;
                    case 2:
                        Menu.DisplayListTasksByUserId();
                        break;
                    case 3:
                        Menu.DisplayTasksFinishedIn2019();
                        break;
                    case 4:
                        Menu.DisplayUsersOlderThan12YearsOld();
                        break;
                    case 5:
                        Menu.DisplaySortedUsersList();
                        break;
                    case 6:
                        Menu.DisplayInfoAboutUser();
                        break;
                    case 7:
                        Menu.DisplayInfoAboutProject();
                        break;
                }

                Menu.Wait();
            } while (true);
        }
    }
}
