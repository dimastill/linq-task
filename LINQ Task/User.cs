﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    class User
    {
        public int Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        [JsonProperty("registered_at")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty("team_id")]
        public Team Team { get; set; }

        public static implicit operator User(long Id)
        {
            string jsonUsers = Data.GetJson($"https://bsa2019.azurewebsites.net/api/Users/{Id}");
            return JsonConvert.DeserializeObject<User>(jsonUsers);
        }
    }
}
