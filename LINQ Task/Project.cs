﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        [JsonProperty("author_id")]
        public User Author { get; set; }
        [JsonProperty("team_id")]
        public Team Team { get; set; }

        public static implicit operator Project(long Id)
        {
            string jsonProjects = Data.GetJson($"https://bsa2019.azurewebsites.net/api/Projects/{Id}");
            return JsonConvert.DeserializeObject<Project>(jsonProjects);
        }
    }
}
