﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Task
{
    class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreateAt { get; set; }
        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }
        public TaskStateModel State { get; set; }
        [JsonProperty("project_id")]
        public Project Project { get; set; }
        [JsonProperty("performer_id")]
        public User Perfomer { get; set; }
    }
}
